<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activites', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titre');
            $table->string('slug');
            $table->text('chapeau');
            $table->string('photo')->default('');
            $table->string('legend')->default('');
            $table->integer('categorie_id')->default(0);
            $table->longText('corps');
            $table->integer('vues')->default(0);
            $table->boolean('etat')->default(0);
            $table->integer('user_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activites');
    }
}
