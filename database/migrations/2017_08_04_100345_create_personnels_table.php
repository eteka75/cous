<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonnelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnels', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nom_prenom');
            $table->string('fonction')->default('');
            $table->string('grade')->default('');
            $table->longText('biographie');
            $table->string('adresse')->default('');
            $table->string('photo')->default('');
            $table->integer('user_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personnels');
    }
}
