<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommuniquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communiques', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titre');
            $table->text('slug');
            $table->longText('contenu');
            $table->text('fichier');
            $table->integer('user_id');
            $table->boolean('etat');
            $table->dateTime('date_limit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('communiques');
    }
}
