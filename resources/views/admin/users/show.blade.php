@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
            <header class="pageheader hidden">
                <h3><i class="fa fa-users"></i> Utilisateurs </h3>
            </header>
            <div class="panel">
                <div class="panel-heading"><h3 class="panel-title">Utilisateurs</h3></div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/users') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/users', $user->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete User',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                
                                <tbody>
                                    <tr>
                                        <td with="10%">
                                            <span class="text-muted">Id : </span>
                                        </td>
                                        <td>{{ $user->id }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text-muted">Nom : </span>
                                        </td>
                                        <td> {{ $user->nom }} </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text-muted">Prénom : </span>
                                        </td>
                                        <td> {{ $user->prenom }} </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text-muted">Sexe : </span>
                                        </td>
                                        <td> {{ $user->sexe=='F'?'Féminin':'Masculin' }} </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <span class="text-muted">E-mail : </span>
                                        </td>
                                        <td> {{ $user->email }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection