<div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', 'Titre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('titre', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fichiers') ? 'has-error' : ''}}">
    {!! Form::label('fichiers', 'Choisier les fichiers', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('fichiers',  ['class' => 'form-control','multiple'=>'multiple']) !!}
        {!! $errors->first('fichiers', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('references') ? 'has-error' : ''}}">
    {!! Form::label('references', 'References', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('references', null, ['class' => 'form-control','rows'=>2]) !!}
        {!! $errors->first('references', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Enrégistrer', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
