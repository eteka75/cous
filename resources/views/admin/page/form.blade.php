@section('css')
<!--<link href="{{asset('assets/vendor/summernote/dist/summernote.css')}}" rel="stylesheet">-->
@endsection
@section('script')
<!--<script src="{{asset('assets/vendor/summernote/dist/summernote.min.js')}}"></script>-->
<!-- include libraries(jQuery, bootstrap) -->
<!--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> -->

<!-- include summernote css/js-->
<!--<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">-->
<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>-->
<!--Select2-->
<link href="{{asset('assets/vendor/select2/select2.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/select2/select2.js')}}"></script>
<!--Summernote-->
<link href="{{asset('assets/vendor/summernote/dist/summernote.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/summernote/dist/summernote.js')}}"></script>
<script type="text/javascript">
$('.select2').select2();
var $editor = $('.summernote');
$editor.summernote({
    disableDragAndDrop: true,
    callbacks: {
        // Clear all formatting of the pasted text
        onPaste: function (e) {
            var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            e.preventDefault();
            setTimeout(function () {
                document.execCommand('insertText', false, bufferText);
            }, 10);
        }
    },
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
//    ['height', ['height']],
        ['fontsize', ['fontsize']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen']],
                //=>, 'superscript', 'subscript'
//    ['help', ['help']]
    ],
    height: 250,
    minheight: 100,
    placeholder: 'Rédigez votre contenu',
    dialogsInBody: true,
    lang: 'fr-FR',
});
</script>
@endsection
<?php
?>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
            {!! Form::label('Page', 'Choisir la page', ['class' => 'col-md-12 control-labels']) !!}
            <div class="col-md-12">
                {!! Form::select('slug',$pages,null, ['class' => 'form-control select2', 'required' => 'required']) !!}
                {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

    </div>
    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
            {!! Form::label('titre', 'Titre de page', ['class' => 'col-md-12 control-labels']) !!}
            <div class="col-md-12">
                {!! Form::text('titre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

    </div>

</div>
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('Photo', 'Photo', ['class' => 'col-md-12 control-labels']) !!}
    <div class="col-md-12">
        {!! Form::file('photo',  ['class' => 'form-control']) !!}
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('has_photo') ? 'has-error' : ''}}">
    <div class="col-sm-3">
        {!! Form::label('Photo', 'Prendre en compte la photo', ['class' => 'col-md-4control-labels']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::radio('has_photo',  0, ['class' => 'form-control','checked'=>TRUE]) !!} Non &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;        
        {!! Form::radio('has_photo', 1,  ['class' => 'form-control']) !!} Oui 

    </div>
    <div class="col-sm-12">
        {!! $errors->first('has_photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div id="cover_summernote">
    <div class="form-group {{ $errors->has('contenu') ? 'has-error' : ''}}">
        <!--{!! Form::label('contenu', 'Contenu', ['class' => 'col-md-12 control-labels']) !!}-->
        <div class="col-md-12">
            {!! Form::textarea('contenu', null, ['class' => 'form-control summernote', 'required' => 'required','rows'=>10]) !!}
            {!! $errors->first('contenu', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-0 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Ajouter aux pages', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
