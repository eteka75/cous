<div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', 'Titre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('titre', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
    {!! Form::label('slug', 'Slug', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('slug', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('chapeau') ? 'has-error' : ''}}">
    {!! Form::label('chapeau', 'Chapeau', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('chapeau', null, ['class' => 'form-control']) !!}
        {!! $errors->first('chapeau', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', 'Photo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('photo', null, ['class' => 'form-control']) !!}
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('legend') ? 'has-error' : ''}}">
    {!! Form::label('legend', 'Legend', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('legend', null, ['class' => 'form-control']) !!}
        {!! $errors->first('legend', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('categorie_id') ? 'has-error' : ''}}">
    {!! Form::label('categorie_id', 'Categorie Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('categorie_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('categorie_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('corps') ? 'has-error' : ''}}">
    {!! Form::label('corps', 'Corps', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('corps', null, ['class' => 'form-control']) !!}
        {!! $errors->first('corps', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('vues') ? 'has-error' : ''}}">
    {!! Form::label('vues', 'Vues', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('vues', null, ['class' => 'form-control']) !!}
        {!! $errors->first('vues', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', 'Etat', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
    <label>{!! Form::radio('etat', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('etat', '0', true) !!} No</label>
</div>
        {!! $errors->first('etat', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
