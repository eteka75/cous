<div class="form-group {{ $errors->has('nom_fac') ? 'has-error' : ''}}">
    {!! Form::label('nom_fac', 'Nom de la faculté', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nom_fac', null, ['class' => 'form-control', 'required' => 'required','placeholder'=>"ex:Faculté des Sciences Economiques et de Gestion"]) !!}
        {!! $errors->first('nom_fac', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('sigle') ? 'has-error' : ''}}">
    {!! Form::label('sigle', 'Sigle', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('sigle', null, ['class' => 'form-control', 'required' => 'required','placeholder'=>"ex:FASEG"]) !!}
        {!! $errors->first('sigle', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Ajouter ', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
