@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')
        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('article'))
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Article {{ $article->id }}</div>
                <div class="panel-body">
                    @include('admin.article.actions')
                    <div class="table-responsive">
                        <table class="table table-borderles">
                            <tbody>
                                <tr><th> Titre </th><td> {{ $article->titre }} </td></tr>
                                <tr><th> Chapeau </th><td> {{ $article->chapeau }} </td></tr>
                                <tr><th> Photo </th><td> {!! $article->photo!=''?'<img src="'.asset($article->photo).'" class="img-responsive img-thumbnail">':'' !!} </td></tr>
                                <tr><th  valign="top"> Contenu </th><td valign="top"> {!! $article->corps !!} </td></tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
