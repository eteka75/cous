@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Vos articles</div>
                <div class="panel-body">
                     @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('article'))
                    <a href="{{ url('/admin/article/create') }}" class="btn btn-success btn-sm" title="Add New Article">
                        <i class="fa fa-plus" aria-hidden="true"></i> Ajouter un article
                    </a>
                    @endif
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/article', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" value="{{(isset($_GET['search']))?$_GET['search']:NULL}}" name="search" placeholder="Rechercher...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Titre</th>
                                    <!--<th>Photo</th>-->
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $nb = 1; ?>
                                @foreach($article as $item)
                                <tr>
                                    <td>{{$nb++}}</td>
                                    <td ><a href="{{ url('/admin/article/' . $item->id) }}">{{ $item->titre }}</a></td>
                                    <!--<td>{!! $item->photo!=''?'<img height="50px"  src="'.asset($item->photo).'">':'' !!}</td>-->
                                    <td nowrap>
                                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('article'))
                                        <a href="{{ url('/admin/article/' . $item->id) }}" title="View Article"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        @endif
                                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('article'))
                                        <a href="{{ url('/admin/article/' . $item->id . '/edit') }}" title="Edit Article"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        @endif
                                        @if (Auth::user()->hasRole('admin') || Auth::user()->can('delete_article'))                                        
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/admin/article', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Article',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                        @endif
                                        @if (Auth::user()->hasRole('admin') || Auth::user()->can('validate_article'))
                                        @if(intval($item->etat)==1)
                                        <a href="{{ url('/admin/article/' . $item->id . '/unvalidate') }}" title="Annuler la validation"><button class="btn btn-default btn-xs"><i class="fa fa-remove" aria-hidden="true"></i> Annuler</button></a>
                                        @else
                                        <a href="{{ url('/admin/article/' . $item->id . '/validate') }}" title="Valider Article"><button class="btn btn-success btn-xs"><i class="fa fa-save" aria-hidden="true"></i> Valider</button></a>
                                        @endif
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $article->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
