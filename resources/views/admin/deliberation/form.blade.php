<?php
$facultes=[];
$facultes= App\Faculte::pluck('nom_fac','id');
//$facultes->prepend('Choose something…', '');;
?>
<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', 'Titre de la délibération', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nom', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('annee_academique') ? 'has-error' : ''}}">
    {!! Form::label('annee_academique', 'Année Academique', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('annee_academique', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('annee_academique', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('faculte') ? 'has-error' : ''}}">
    {!! Form::label('faculte', 'Faculté', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="input-group">
            
        {!! Form::select('faculte',$facultes, null, ['class' => 'form-control select2', 'required' => 'required']) !!}
        <span class="input-group-addon rond0"><a href="{{url('admin/faculte/create')}}">Nouveau</a></span>
        </div>
        {!! $errors->first('faculte', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('annee') ? 'has-error' : ''}}">
    {!! Form::label('annee', 'Annee', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('annee', ['all'=>'Toutes les années',1=>'1ère Année',2=>'2ième Année',3=>'3ième Année'],null,['class' => 'form-control', 'required' => 'required','required'=>'true']) !!}
        {!! $errors->first('annee', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', 'Type', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('type',[''=>"Sélectionnez","Bourse"=>'Bourse',"Secours"=>'Secours',"Frais de mémoire"=>'Frais de mémoire','Autres'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($submitButtonText) && $submitButtonText !="Mettre à jour")
<div class="form-group {{ $errors->has('fichier') ? ' has-error' : ''}}">
    {!! Form::label('fichier', 'Fichier de la délibération (format PDF): ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('fichier',  ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('fichier', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
<div class="form-group {{ $errors->has('fichier') ? ' has-error' : ''}}">
    {!! Form::label('fichier', 'Fichier de la délibération (format PDF): ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('fichier',  ['class' => 'form-control']) !!}
        {!! $errors->first('fichier', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
<div class="form-group {{ $errors->has('info') ? ' has-error' : ''}}">
    {!! Form::label('info', 'Autres détails: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('info',null,  ['class' => 'form-control','rows'=>3]) !!}
        {!! $errors->first('info', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', 'Etat de la délibération', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('etat', '1',true) !!} Publiée maintenant</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('etat', '0', false) !!} Non publiée</label>
        </div>
        {!! $errors->first('etat', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Enrégistrer', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
