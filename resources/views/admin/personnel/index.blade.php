@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Personnel</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/personnel/create') }}" class="btn btn-success btn-sm" title="Ajouter un Personnel">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/personnel', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Rechercher...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                       <th>Personnels</th>
                                       <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1?>
                                @foreach($personnel as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <b>{{ $item->nom_prenom }}</b>
                                            <br><small class="text-muted">{{ $item->fonction }}
                                                | {{ $item->grade }}
                                            </small>
                                        </td>
                                        <td width="25%" nowrap>
                                            <a href="{{ url('/admin/personnel/' . $item->id) }}" title="Voir Personnel"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Voir</button></a>
                                            <a href="{{ url('/admin/personnel/' . $item->id . '/edit') }}" title="Editer Personnel"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/personnel', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Supprimer Personnel',
                                                        'onclick'=>'return confirm("Confirmer Suppression?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $personnel->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
