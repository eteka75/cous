@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Galerie</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/galerie/create') }}" class="btn btn-success btn-sm" title="Ajouter à la Galerie">
                            <i class="fa fa-plus" aria-hidden="true"></i> Nouveau
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/galerie', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Rechercher...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <?php $i=1; ?>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>N°</th><th>Titre</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($galerie as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <!--<td>{{ $item->image }}</td>-->
                                        <td>
                                            {{ $item->titre }}<br><small>{{ $item->description }}</small>
                                        </td>
                                        <td width="20%" nowrap>
                                            <a href="{{ url('/admin/galerie/' . $item->id) }}" title="Voir Galerie"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Voir</button></a>
                                            <a href="{{ url('/admin/galerie/' . $item->id . '/edit') }}" title="Editer Galerie"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/galerie', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Supprimer Galerie',
                                                        'onclick'=>'return confirm("Confirmer la suppression?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $galerie->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
