@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Galerie {{ $galerie->id }}</div>
                <div class="panel-body">

                    <a href="{{ url('/admin/galerie') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/admin/galerie/' . $galerie->id . '/edit') }}" title="Edit Galerie"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/galerie', $galerie->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Galerie',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>N°</th>
                                    <td>{{ $galerie->id }}</td>
                                </tr>
                                <tr>
                                    <th> Images </th>
                                    <td>
                                        <div class="clearfix">


                                            <?php
                                            $tab_img = explode(';', $galerie->images);
                                            foreach ($tab_img as $img) {
                                                if (!empty($img)) {
                                                    ?>
                                                    <div class="col-sm-3 pad0 pad15">
                                                        <a data-lightbox="roadtrip" data-lightbox="image-{{$galerie->id}}" data-title="{{ isset($galerie->titre) ? $galerie->titre : '' }}" href="{{asset($img)}}"><img src="{{asset($img)}}" alt="{{asset($img)}}" class="img-responsive img-thumbnail"></a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th> Titre </th>
                                    <td> {{ $galerie->titre }} </td>
                                </tr>
                                <tr>
                                    <th> Description </th>
                                    <td> {{ $galerie->description }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
