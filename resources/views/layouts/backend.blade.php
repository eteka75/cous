<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
        <title>@yield('title', " |  COUS-Parakou, Administration") </title>
        <link rel="stylesheet" href="{{asset('assets/css/template-cms.odace.css')}}">
        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="{{asset('assets/vendor/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
        @yield('css')
        <!--<script src="{{asset('js/jquery.min.js')}}"></script>-->
        <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"  ></script>
        <!-- Scripts -->
        <script>
            window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
        </script>
        <style>
            .form-control{
                box-shadow: none;
                border-radius: 0;
            }
        </style>
    </head>
    <body>
        <nav id="top-link" class="topBar">
            <div class="container">
                <div class="col-sm-8">
                    <ul class="list-inline cous_name pull-left hidden-sm hidden-xs">
                        <li><span class="text-red_ text-uppercase">Centre des Oeuvres Universtaires et Sociales de Parakou</li>
                    </ul>
                </div>
                <div class="col-sm-4 ">
                    @if(Auth::user())
                    <div class="dropdown pull-right">
                        <button class="btn btn-xs btn-defaults no-bg text-white bge rond0 dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            {{Auth::user()->prenom ." ".Auth::user()->nom}}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="{{url('profile')}}">Mon profil</a></li>
                            <li><a href="{{url('/profile/information')}}">Mes informations</a></li>
                            <li><a href="{{url('/profile/activity')}}">Activités du compte</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('logout') }}"
                                   class=""
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    {{ trans('profileLang::profile.sidebar.logout') }}
                                </a></li>

                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </nav><!--=========-TOP_BAR============-->

        <!--=========MIDDEL-TOP_BAR============-->

        <div  class="middleBar">
            <div class="container">
                <div class="row display-table">
                    <div class="col-sm-3 col-xs-4 vertical-align text-left">
                        <a href="/"> <img width="" id="logo" class="img-responsive" src="{{asset('assets/images/logo_cous_parakou.png')}}" alt=""></a>
                    </div>
                    <!-- end col -->
                    <div class="col-sm-6 col-md-7 col-xs-8 vertical-align text-center">

                    </div>
                    <!-- end col -->
                    <div class="col-sm-4 col-md-3 vertical-align header-items hidden-xs">

                    </div>



                    <!-- end col -->
                </div>
                <!-- end  row -->
            </div>
        </div>


        <nav class="navbar navbar-main navbar-default" role="navigation" style="opacity: 1;">
            <div class="container">
                <!-- Brand and toggle -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-1">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>             
                </div>

                <!-- Collect the nav links,  -->
                <div class="collapse navbar-collapse navbar-1" style="margin-top: 0px;">            
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><i class="fa fa-home hidden-xs"></i><span class="visible-xs">Accueil</span></a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <style>
            body{
                font-family: 'Segoe UI','Roboto','Open Sans','Source Sans Pro',sans-serif;
            }
            #logo{max-height: 80px;padding-bottom: 10px}
            .nav>li>a {
                color: #164b79;
                padding: 4px 15px;
            }
            .mtop2{margin-top: 2px;}
            .nav>li.active>a,.nav>li>a:focus, .nav>li>a:hover {
                text-decoration: none;
                background-color: #e4201a;
                color: #ffffff;
            }
            .panel-default>.panel-heading{
                background: #fff;
                font-weight: bold;
                font-size: 18px;
                border-bottom: 0px solid;
                padding: 5px 15px;
                text-transform: uppercase;
                font-weight: 600;
                color: #fff;
                background: #0c2a44;
                /*                background: #e0e0e0;*/
            }
            .panel{
                border-width: 0px;
                border-radius: 0px;
                box-shadow: none;
            }
            #top-link .text-red{color: #e8d017;font-weight: 300}
            #top-link{ 
                color: #dedede;
                background: #ffffff;
                background: #0c2a44;
                border-bottom: 4px solid #e12119;
                font-size: 13px;
                padding: 0px 0 0;
            }
            #top-link .cous_name{
                margin:5px 0;padding: 0
            }
            .topBar ul.topBarNav li ul {
                background-color: #ffffff;
                position: absolute;
                top: 42px;
                left: auto;
                min-width: 10px;
                right: 4px;
                border-radius: 0px;
                border: solid 0px;
                margin-right: -4px;
                padding: 0;
                list-style-type: none;
                z-index: 9999;
                -webkit-transition: all 0.1s ease-in-out;
                -moz-transition: all 0.1s ease-in-out;
                -ms-transition: all 0.1s ease-in-out;
                -o-transition: all 0.1s ease-in-out;
                transition: all 0.1s ease-in-out;
                -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.08);
                -moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.08);
                -ms-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.08);
                box-shadow: 0 1px 5px rgba(0, 0, 0, 0.08);
            }
            .topBar ul.topBarNav li ul li {
                display: block;
                line-height: 30px;
                width: 100%;
                border: none;
            }

            .topBar ul.topBarNav li a {
                display: block;
                padding-left: 12px;
                padding-right: 12px;
            }
            .topBar .dropdown-menu > li > a {
                display: block;
                padding: 6px 20px;
                clear: both;
                font-weight: normal;
                line-height: 1.42857143;
                color: #878c94; 
                font-size:15px;
                transition: ease .2s all;
                white-space: nowrap;
            }
            .topBar .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
                color: #e4201a;
                text-decoration: none;
                background-color: rgba(0, 0, 0, 0.02);
            }


            /*/////////////////////////middleBar//////////////////////*/

            .middleBar {
                padding: 10px 0 0 0;
                margin-bottom: 15px;;min-height: 70px;
                border-bottom: 1px solid rgba(0, 0, 0, 0.08);
                background: #fff;
            }

            @media (min-width: 767px){
                #quote-carousel .carousel-control.left {
                    left: -60px;
                }
                #quote-carousel .carousel-control.right {
                    right: -60px;
                }
                .display-table {
                    display: table;
                    width: 100%;
                }
                .navbar-nav>li:hover{background: #fff;border-top: 2px solid #e4201a;margin-top: -2px;stransition: ease .2s all;}
                .vertical-align {
                    display: table-cell;
                    vertical-align: middle;
                    float: none;
                }
                .grid-space-1 div[class*="col-"] {
                    padding: 0 1px;
                }
            }
            .input-lg{height: 40px}
            .btn-group-lg>.btn, .btn-lg{font-size: 14px;}
            @media (max-width: 767px){
                .cfoot ul>li{
                    padding: 5px 10px;
                    display: block;
                    border-bottom: 1px solid #444444;
                }
                i.fa.fa-angle-down.ml-5{
                    float: right;
                }
                div[class^="col-"] {
                    margin-bottom: 0px;
                    margin-top: 3px;
                }
                #logo{
                    max-height: 55px;
                }
            }

            .middleBar .form-control {
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                -ms-box-shadow: none;
                box-shadow: none;
                -webkit-border-radius: 0;
                -moz-border-radius: 0;
                -ms-border-radius: 0;
                border-radius: 0;
                border: 1px solid rgba(0, 0, 0, 0.15);
                background-color: #ffffff;
                -webkit-transition: all 0.1s ease-in;
                -moz-transition: all 0.1s ease-in;
                -ms-transition: all 0.1s ease-in;
                -o-transition: all 0.1s ease-in;
                transition: all 0.1s ease-in;
            }

            .form-control.input-lg {
                font-size: 15px;
            }

            .middleBar .btn.btn-default {
                color: #ffffff;
                background-color: #e4241c;
                border: 1px solid #e4201a;
                border-radius: 0px;
            }
            .middleBar .header-items .header-item {
                display: inline-block;
            }

            .middleBar .header-items .header-item a {
                position: relative;
                display: block;
                border: 1px solid rgba(0, 0, 0, 0.08);
                width: 40px;
                height: 40px;
                line-height: 40px;
                -webkit-border-radius: 100%;
                -moz-border-radius: 100%;
                -ms-border-radius: 100%;
                border-radius: 100%;
                text-align: center;
                color: #35404f;
            }
            .middleBar .header-items .header-item a sub {
                position: absolute;
                bottom: -8px;
                right: -8px;
                width: 20px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                background-color: #0cd4d2;
                -webkit-border-radius: 100%;
                -moz-border-radius: 100%;
                -ms-border-radius: 100%;
                border-radius: 100%;
                color: #ffffff;
                font-size: 9px;
                -webkit-transition: all 0.2s ease-in;
                -moz-transition: all 0.2s ease-in;
                -ms-transition: all 0.2s ease-in;
                -o-transition: all 0.2s ease-in;
                transition: all 0.2s ease-in;
            }
            .middleBar .header-items .header-item a:hover {
                background-color: #0cd4d2;
                color: #ffffff;
            }

            /*/////////////////////////navbar-default///////////////*/

            .navbar-default {
                display: none;
                border: none;
            }

            .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
                color: #e5201a;
                background-color: transparent;
            }
            .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:after {
                border-top:solid 0px #0cd4d2;

            }
            .navbar-default .dropdown-menu {
                padding: 0;
                font-size: 14px;
                background-color: #ffffff;
                color: #878c94;
                border: none;
                -webkit-border-radius: 0;
                -moz-border-radius: 0;
                -ms-border-radius: 0;
                border-radius: 0;
                -webkit-box-shadow: 0px 4px 5px rgba(0, 0, 0, 0.08);
                -moz-box-shadow: 0px 4px 5px rgba(0, 0, 0, 0.08);
                -ms-box-shadow: 0px 4px 5px rgba(0, 0, 0, 0.08);
                box-shadow:0px 4px 5px rgba(0, 0, 0, 0.08);
                -webkit-transition: all 0.1s ease-in;
                -moz-transition: all 0.1s ease-in;
                -ms-transition: all 0.1s ease-in;
                -o-transition: all 0.1s ease-in;
                transition: all 0.1s ease-in;
            }

            .dropdown-menu > li > a {
                display: block;
                padding: 6px 20px;
                clear: both;
                font-weight: normal;
                line-height: 1.42857143;
                color: #878c94;
                white-space: nowrap;
            }
            .dropdown-menu > li > a:hover{
                color: #ffffff;
            }


            /***********************
                 megaDropMenu
            ////////////////////////////*/

            .navbar-default .container{
                position:relative;
            }
            .navbar-default .navbar-collapse li.dropdown.megaDropMenu {
                position: static;
            }

            .navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu{
                width: 100%;
            }
            .dropdown-menu.row li>ul {padding: 0}
            .navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu .dropdown-header{
                padding: 10px 0px 5px 15px;
                font-size: 16px;
                text-transform: uppercase;
                /* text-align: center; */
                color: #e4241c;
            }
            .navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu li ul li {list-style-type: none;padding: 0 0px;}
            .navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu li ul li a{
                display:block;color:#2d2d2d;font-size: 14px;text-decoration:none;padding:8px 15px;
            }
            .navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu li ul li a:hover{
                color: #ffffff;
                text-decoration: none;
                background-color: #e4241c;
            }

            .navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu img{
                display: block;
                max-width: 100%;
                padding: 20px;
            }  
        </style>
        <nav class="navbar navbar-default navbar-static-top hidden">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'LERF') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/admin') }}">Tableau de board <span class="sr-only">(current)</span></a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->prenom }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @if (Session::has('flash_message'))
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <div class="alert bg-primary alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('flash_message') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @yield('content')

        <div class="pad15 bgf text-center text-xs">
            &copy; {{ date('Y') }} COUS-PARAKOU. Powered by <a href="https://www.facebook.com/wilfried.eteka/">ETEKA Wilfried</a>

        </div>

        <!-- Scripts -->
        
        <script src="{{asset('/js/app.js')}}"></script>
        <link rel="stylesheet" href="{{asset('assets/vendor/lightbox/css/lightbox.min.css')}}">
        <script src="{{asset('assets/vendor/lightbox/js/lightbox.min.js')}}"></script>


        <script type="text/javascript">
                                           $(function () {
                                               // Navigation active
//                                               $('ul.navbar-nav a[href="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"]').closest('li').addClass('active');
                                           });
        </script>
        <script src="{{asset('assets/plugins/pjax/jquery.pjax.js')}}"></script>    
        @yield('script')
    </body>
</html>
