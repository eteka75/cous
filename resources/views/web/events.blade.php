@extends('layouts.web')
@section('title')
Evènements 
@endsection
@section('content')
@if(isset($events))
<div class="container bgwhite margin-top-5  ">
    <div class="row">
        <div class="col-lg-8 col-sm-8">
            <h3 class="light  page-header up-page-header">Evènements</h3>
            <div class="pad10 bgwhite shadow1 main-content">
                <div class="menu-html-content mtop-10">

                    <div class="row ">
                        <div class="col-sm-12 margin-top-10">
                            @if (isset($events) && $events->count())
                            <ul class='list-unstyled '>
                                @foreach ($events as $event) 
                                <li >
                                    <div class="row">
                                        <div class="col-sm-2 text-center text-muted ">
                                            <i class="glyphicon glyphicon-calendar f60"></i>
                                            <span class="label label-danger fnormal">{{date('d/m/Y',strtotime($event->date_event))}}</span>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class=" pad10 f12">
                                                <h5><a href='{{url('event/'.$event->slug)}}'>{{$event->titre}} </a>
                                                    <small class="pull-right">
                                                        <date class="text-sm f11 text-muted">{{date('d/m/Y H:i',strtotime($event->created_at))}}</date>
                                                    </small>
                                                </h5>
                                                @if(date('Y-m-d', strtotime($event->date_event)) == date('Y-m-d'))
                                                <small class="label pull-right fnormal label-success text-uppercase">Aujourd'hui</small>
                                                @elseif(date('Y-m-d', strtotime($event->date_event)) > date('Y-m-d'))
                                                <small class="label pull-right fnormal label-success text-uppercase">à venir</small>
                                                @else
                                                <small class="label pull-right fnormal label-danger text-uppercase">passé</small>
                                                @endif
                                                <div class=' text-muted padding-bottom-10 padding-top-5'>
                                                    <a href='{{url('event/'.$event->slug)}}'>{{substr($event->detail,0,150).'...'}}</a>
                                                </div>
                                                <ul class="list-inline">
                                                    <li>
                                                        {!!$event->date_event!=''?'<i class="glyphicon glyphicon-calendar"></i> : '.date('d/m/Y',strtotime($event->date_event)):''!!} 
                                                    </li>
                                                    <li>
                                                        {!!$event->date_event!=''?'<i class="glyphicon glyphicon-time"></i> : '.date('H:i',strtotime($event->date_event)):''!!} 
                                                    </li>
                                                    <li>
                                                        {!!$event->lieu!=''?'<i class=" glyphicon glyphicon-home"></i> : '.$event->lieu:''!!}
                                                    </li>
                                                    <li>
                                                        {!!$event->auteur!=''?'<i class=" glyphicon glyphicon-glass"></i> : '.$event->auteur:''!!}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class='hr-xs'>
                                </li>
                                @endforeach
                            </ul>
                            <div class="text-right">
                                {{$events->links()}}

                            </div>
                            @endif
                        </div> 
                    </div>
                    @if($events->count()<=0)
                    <div class="clearfix text-center">
                        <br><br><br>
                        <h3 class="pad15 text-muted">Aucun évènement pour le moment</h3>
                        <img src="{{asset('assets/images/resources/search.png')}}" alt="Aucun résultat">
                       
                    </div>
                    <div class="text-center">
                         <a class="btn btn-default  btn-sm" href="{{"login"}}">Suggérer un évènement</a>
                    </div>
                    @endif
                </div> 
            </div>
        </div>
        <div class="col-lg-4 col-sm-4   ">
            @include("includes/infosright");
        </div>
    </div>
</div>
@endif
@endsection
