@extends('layouts.web')
@section('title')
{!! isset($page->titre) ? $page->titre : '' !!}
@endsection
@section('content')

<div class=" container ">
    <div class="rows">
        <div class="col-lg-8 col-sm-8">
            <h2 class="light pad10_0  m0 page-header">Contactez-nous </h2>
            <div class="pad15 bordere ronde"> 
            <div class="pad10 bgwhite  main-content">
                <div class="menu-html-content mtop-10">
                   {!!isset($page->contenu) ? ($page->contenu) : ''!!}
                <hr> 
                
                <div class="headline"><h2 class="light text-uppercase text-center"><i class="icon-bubbles"></i> Ecrivez-nous</h2></div><br>   
                      
                        <div class="form-group ">
                            <label for="name" class="sr-only">Nom et prénoms</label>
                            <input id="name" class="form-control rond0" placeholder="Nom et prénoms" type="text">
                        </div>
                        <div class="form-group ">
                            <label for="email" class="sr-only">Email</label>
                            <input id="email" class="form-control rond0" placeholder="Email" type="email">
                        </div>
                        <div class="form-group ">
                            <label for="phone" class="sr-only">Téléphone</label>
                            <input id="phone" class="form-control rond0" placeholder="95000000" type="text">
                        </div>
                        <div class="form-group ">
                            <label for="message" class="sr-only">Message</label>
                            <textarea name="" id="message" cols="30" rows="5" class="form-control rond0" placeholder="Message"></textarea>
                        </div>
                        <div class="form-group ">
                            <input class="btn btn-primary rond0 btn-lg" value="Envoyer le message" type="submit">
                        </div>
                    </div>
                    </div>
                </div> 
            </div>

        </div>
        <div class="col-lg-4 col-sm-4 ">
           @include('partials.right')

        </div>
    </div>
</div>

@endsection
