@extends('layouts.web')
@section('title')
{!! isset($page->titre) ? $page->titre : 'Liste du personnel' !!}
@endsection
@section('css')
<style type="text/css">
    .list, .block .content.list{padding: 0px; list-style: none;}
    .list,.list .list-item,.list .list-title{display: block; width: 100%; float: left;}
    .list .list-title{color: #FFF; font-weight: bold; font-size: 12px; text-transform: uppercase; text-decoration: none; padding: 10px;}
    .list .list-item{ color: #333; position: relative; overflow: hidden;text-align: left}
    .list .list-item{background: #f7f7f7;}
    .list .list-item:nth-child(2n+1){background: #DEE4E5;}
    /*.list .list-item:hover{background: #FFF;}*/            

    .list-default .list-item{background: rgba(0,0,0,0.2); color: #FFF; margin-bottom: 3px; overflow: auto;}    
    .list-default .list-item:nth-child(2n+1){background: rgba(0,0,0,0.3);}
    .list-default .list-item:hover{background: rgba(0,0,0,0.1);}
    .list-default .list-item .list-text{float: left;}
    .list-default .list-item .list-title{padding-bottom: 0px;}
    .list-default .list-item .list-text img{margin: 0px 5px 0px 0px;}                

    .list .list-item .list-datetime,
    .list .list-item .list-info{float: left; padding: 8px 10px 8px 10px;}
    .list .list-item .list-datetime{width: 65px;}
    .list .list-item .list-datetime .date{font-size: 14px; color: #BA3450; text-align: center; font-weight: bold;}
    .list .list-item .list-datetime .time{font-size: 11px; color: #8F95A1; text-align: center; line-height: 16px;}

    .list .list-item .list-text{padding: 8px 15px 8px 20px;}
    .list .list-item .list-text .list-text-name{font-size: 16px;font-weight: bold; color: #333;}
    .list .list-item .list-text p{margin: 5px 0px; font-size: 11px; color: #888D9A; line-height: 16px;}
    .list .list-item .list-text .list-text-info{font-size: 11px; color: #666; margin-top: 3px; line-height: 14px;}


    .list .list-item .list-controls{
        border-radius: 0 0 0 30px;
        position: absolute;
        /*height: 40px;*/ 
        width: 50%; color: #fefefe; height: 70px; 
        right: -50%; top: 0px; 
        padding: 5px 10px/*; background: rgba(225,90,0,0.9)*/;background: #376577; transition: 200ms; line-height: 25px;}
    .list .list-item:hover .list-controls{transition: 200ms; right: 0;}
    .list .list-item .list-controls a{margin-right: 10px;}                        
    .list .list-item .list-controls a,.list .list-item .list-controls .fa{color: #FFFFFF}
    .list.list-contacts .list-controls{height: 56px ;}

    .list-default .list-item .list-text p{font-size: 12px; color: #FFF; margin-bottom: 3px; margin-top: 0px; padding-top: 0px;}        

    .list.list-contacts{background: transparent;}
    .list.list-contacts .list-item{background: rgba(0,0,0,0.2); margin-bottom: 1px; color: #FFF; margin-bottom: -1px;}
    .list.list-contacts .list-item:hover,
    .list.list-contacts .list-item.active{background: rgba(0,0,0,0.1);}
    .list.list-contacts .list-item .list-text .list-text-name{color: #FFF;}
    .list.list-contacts .list-item .list-text p,.list.list-contacts .list-item .list-text .list-text-info{color: #EEE;}
    .list.list-contacts .list-item:last-child{margin-bottom: 0px;}

    .list-status{width: 10px; height: 10px; position: absolute; right: 10px; top: 50%; margin-top: -5px;
                 -moz-border-radius: 50%; -webkit-border-radius: 50%; border-radius: 50%;}       
    .list-status-online{background: #86D435;}
    .list-status-offline{background: #EF372E;}
    .list-status-away{background: #FF9806;}


    .list .list-item .progress{margin: 3px 0px 2px;}
    .list-info>img{
        width:100px;
        height: 100px;
    }
</style>
@endsection
@section('content')

<div class="container">
    <div class="row">
        <div class="pad15">
        <div class="col-lg-8 col-sm-8">
            <div class="headline"><h2 class="light rs   text-center mtop20">Le personnel du COUS Parakou</h2></div>
            <div class="pad0 mtop10 bgwhite  main-content">
                <div class="menu-html-content mtop-10">
                    @if(isset($personnels) && $personnels!=NULL)
                    <div class="content list">
                        @foreach($personnels as $p)
                        <div class="list-item">
                            <!--                            <div class="list-datetime">
                                                            <div class="date">26.08</div>
                                                            <div class="time">12:12 am</div>
                                                        </div>-->
                            <div class="list-info">
                                @if($p->photo!=NULL)
                                <img src="{{asset($p->photo)}}" class="img-circle img-thumbnail">
                                @endif
                            </div>
                            <div class="list-text ">
                                <h3 class="bold">{{$p->nom_prenom}}</h3>
                                 @if($p->fonction!=NULL)
                                 <small>{!!$p->fonction!!}</small>
                                 @endif
<!--                                <ul class="list-unstyled text-sm  pad0 m0">
                                    <li class="bold text-uppercase "><span class="text- text-muted">Spécialité : </span>{{$p->specialite}}</li>
                                    <li class="text-muted">{{$p->categorie}}</li>
                                </ul>-->
                                <div class="lcars pad10_0">
                                    @if($p->adresse!=NULL)
                                    <i class="fa btn btn-primary fa-phone"></i> Addresse : {!!$p->adresse!!}
                                    @endif
                                </div>
                                

                            </div>
                                                        
                        </div>                        

                        @endforeach
                        <div class="text-right">
                            {{$personnels->links()}}
                        </div>
                    </div>
                    @else
                    <div class="well text-center margin-top-30 text-muted">
                        <i class="fa fa-users  huge-data-fa margin-top-30" ></i>
                        <h2 class="light" >Aucun personnel disponible</h2 >
                    </div>
                    @endif
                </div> 
            </div>
        </div>
        
        <div class="col-lg-4 col-sm-4">
            @include('partials.right')

        </div>
            </div>
    </div>
</div>

@endsection
