@extends('layouts.web')
@section('title')
Documents administratifs
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection
@section('title')
Documents administratifs
@endsection
@section('content')

<div class="container bgwhite margin-top-5  ">
    <div class="row">
        <div class="col-lg-8 col-sm-8">
            <h3 class="light  page-header up-page-header">Documents administratifs</h3>
            <form method="get" action="{{route('search')}}">
                <div class="input-group">
                    <input type="hidden" name="type" value="document">
                    <input type="text" name="q" class="form-control  search-control" placeholder="En quoi pouvons nous vous aider... ?">
                    <span class="input-group-btn">
                        <button class="btn btn-default search-btn"><i class="icon-magnifier"></i></button>
                    </span>
                </div>
            </form>
            <div class="pad10 bgwhite shadow1 main-content">
                <div class="menu-html-content mtop-10">


                    @if (isset($documents)) 
                    <ul class='list-unstyled'>
                        @foreach ($documents as $document) 

                        <li><img height='15px' alt='{{ $document->type_doc }}' src='{{asset("assets/uploads/icones/pdf.png")}}'>  
                            @if ($document->date_emission != "0000-00-00") 
                            <small class='text-xs text-muted pad0 text-right'><b>Date d'émission : </b> {{ date('d/m/Y', strtotime($document->date_emission)) }}</small>
                            <a class="btn btn-default pull-right no-border" title='Télécharger le fichier' target='_blanck' href='{{ ($document->url)?$document->url:'' }}' >
                                <i class="fa fa-download"></i>
                                <a>
                                    @endif
                                    <small class='text-xs pad0'><br>
                                        <a title='Télécharger le fichier' target='_blanck' href='{{ ($document->url)?$document->url:'' }}' >
                                            {{ substr($document->objet, 0, 255) }}...
                                        </a>
                                    </small><hr class='hr-xs'>
                                    </li>
                                    @endforeach
                                    </ul>
                                    @endif
                                    <div class="text-right">
                                        {{$documents->links()}}
                                    </div>

                                    </div> 
                                    </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4   ">
                                        @include("includes/infosright");
                                    </div>
                                    </div>
                                    </div>

                                    @endsection
