@extends('layouts.web')
@section('title')
{!! isset($page->titre) ? $page->titre : '' !!}
@endsection
@section('css')

@endsection
@section('content')
<style type="text/css">
  

</style>
<div class="  ">
    <div class="row">
        <div class="col-lg-9 col-sm-9">
            <h2 class="light pad10_0  m0 page-header">Nos publications</h2>
            <div class="pad0 mtop10 bgwhite  main-contents">
                <div class="menu-html-content mtop-10">
                    @if(isset($publications) && $publications->count() )
                    <div class="mlist">
                        @foreach($publications as $p)
                        <div class="doc-line">
                            <h3 class="bold text-success-v">
                                <span class="text-danger">[{{$p->annee}}]</span>
                                {{$p->auteur}} </h3>
                            <b class="text-uppercase">{{$p->type}}</b>
                            <div class="text-muted text-sm">
                                {{$p->titre}},{{$p->references}}
                                <ul class="list-unstyled text-sm text-muted pad0 m0">
                                    <li class="bold text-uppercase ">{{$p->categorie}}</li>
                                    <li>{{$p->categorie}}</li>
                                </ul>
                                

                            </div>                            
                        </div>                        

                        @endforeach
                    </div>
                    <div class="text-right">
                        {{$publications->links()}}
                    </div>
                    @else
                    <div class="well text-center margin-top-30 text-muted">
                        <i class="fa fa-graduation-cap  huge-data-fa margin-top-30" ></i>
                        <h2 class="light" >Aucune publication pour le moment</h2 >
                    </div>
                    @endif
                </div> 
            </div>
        </div>
        <div class="col-lg-3 col-sm-3 Rpanel">
            @include('partials.right')

        </div>
    </div>
</div>

@endsection
