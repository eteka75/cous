@extends('layouts.web')
@section('title')
Annonce : {{$annonce->titre}} 
@endsection
@section('content')
@if(isset($annonce))
<div class="container bgwhite margin-top-5  ">
    <div class="row">
        <div class="col-lg-8 col-sm-8">
            <h3 class="light  page-header up-page-header">{{$annonce->titre}}</h3>
            <div class="pad10 bgwhite shadow1 main-content">
                <div class="menu-html-content mtop-10">
                     @if(strlen($annonce->sous_titre)>0)
                    
                    <blockquote>
                        {{$annonce->sous_titre}}
                    </blockquote>
                    @endif
                    <div>
                        {!!$annonce->detail!!}
                    </div>
                </div> 
            </div>
        </div>
        <div class="col-lg-4 col-sm-4   ">
            @include("includes/infosright");
        </div>
    </div>
</div>
@endif
@endsection
