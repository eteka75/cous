@extends('layouts.web')
@section('title')
{!! isset($page->titre) ? $page->titre : '' !!}
@endsection
@section('content')
@if(isset($page->photo) && $page->photo!='')

@endif
<style>
    .heading-page{
        height: 150px;
        overflow: hidden;
    }
    .heading-page>img{
        /*max-height: 100%;*/
        width: 100%;
    }
    img{
        max-width: 100%;
    }
</style>
<!--<div class="heading-page">
    <img src="{{asset($page->photo)}}" class="" alt="{{$page->titre}}"/>
</div>-->
<div class=" container ">
    <div class="row">
        <div class="col-sm-12 main-contents">
            <div class="col-lg-8 col-sm-8">
                <div class="headline"><h2 class="light rs   text-center mtop20"> {!! isset($page->titre) ? $page->titre : '' !!}</h2></div>                         

                <!--<h2 class="light   m0 rss page-header "></h2>-->
                <div class=" bgwhite">
                    <div class="menu-html-content mtop-10">
                        @if(isset($page->photo) && $page->photo!='')
                        <a href="{{asset($page->photo)}}" data-lightbox="roadtrip" data-lightbox="image-{{$page->id}}" data-title="{{ isset($page->titre) ? $page->titre : '' }}">
                        <img  src="{{asset($page->photo)}}" class="img-responsive z-in" alt="{{$page->titre}}"/><hr>
                       
                        </a>
                        @endif
                        {!!isset($page->contenu) ? ($page->contenu) : ''!!}
                    </div> 
                </div>
            </div>
        
        <div class="col-lg-4 col-sm-4 Rpanel">

            @include('partials.right')
        </div>
            </div>
    </div>
</div>
<script>
    $(function(){
        $('*').find('img').addClass('img-responsive');
        $('body').find('img').attr('data-lightbox','hh');
    })
</script>
@endsection
