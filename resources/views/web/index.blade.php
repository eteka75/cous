@extends('layouts.web')
@section('title')
Université de Parakou | Le site officiel
@endsection
@section('content')
<?php
$slides = \App\Slide::where('etat', '1')->orderBy('created_at')->take(5)->get();
//dd($slides);
$mot = App\Page::where('slug', 'mot-du-recteur')->first();
$actus = App\Article::where('etat', '1')->orderBy('created_at')->take(12)->get();
//dd($actus);
$docs = App\Document::orderBy('created_at')->take(12)->get();
$historique = \App\Page::where('slug', 'historique')->first();
$mv = \App\Page::where('slug', 'mission-et-visions')->first();
$admission = \App\Page::where('slug', 'admission')->first();
//dd($admission);
//echo $mot->titre;
?>

<div class="container bgwhite padding-top-10">
    @if($slides->count())
    <div class="row margin-bottom-20">
        <div class=" col-lg-12 " >
            <div class=" " >
                <div id="up-slide-div-cover" class="slide-content bgd" >
                    <!-- Loading Screen -->
                    <div data-u="loading" id="up-slide-loading">
                        <div id="up-slide-preload1"></div>
                        <!--div id="up-slide-preload2"></div-->
                    </div>
                    <div data-u="slides" class="up-data-slide" >
                        @foreach($slides as $slide)
                        <div>
                            <img data-u="image" src="{{asset($slide->Photo)}}" class="img-responsive" alt="Photo lancement IFSIO 2016" />
                            <div class="slide-msg">
                                <div class="htext text-uppercase"><h1>{{$slide->titre}}</h1></div>
                                <p class="mtext">{{$slide->sous_titre}}<br>
                                </p>
                            </div>
                        </div>
                        @endforeach




                    </div>
                    <!-- Bullet Navigator -->
                    <div data-u="navigator" class="up-lslide hidden-xs" style="bottom:16px;right:16px;" data-autocenter="1">
                        <div data-u="prototype" style="width:16px;height:16px;"></div>
                    </div>
                    <!--div data-u="navigator" class="up-lslide hidden-xs" style="bottom:16px;right:16px;" data-autocenter="1">
                            <div data-u="prototype" style="width:16px;height:16px;"></div>
                        </div-->
                    <!-- Arrow Navigator -->
                    <span data-u="arrowleft" class="prevArrowbt" style="top:0px;left:0px;width:30px;height:46px;" data-autocenter="2"></span>
                    <span data-u="arrowright" class="prevNextbt" style="top:0px;right:0px;width:30px;height:46px;" data-autocenter="2"></span>
                </div>
            </div>
        </div>

    </div>
    @endif
    <div class=" content faq-page ">
        <div class=" ">
            <div class="col-md-8">
                <!-- Blog Grid -->
                <?php
                if (count($mot['contenu'])) {
                    //dd($mot);
                    ?>
                    <div class="row ">
                        <div class="col-sm-4 col-xs-12 mauto ">
                            <img class="img-responsive img-circle xs-not-circle pad50" src="{{asset($mot['photo'])}}" alt="recteur de l'Université de Parakou">
                            <p class="text-center text-sm text-muted police12 "> Prof. Prosper GANDAHO</p>
                        </div>
                        <div class="col-sm-8  col-xs-12">

                            <div class="up-grid">
                                <h3 class="">{{ isset($mot->titre) ? ($mot->titre) : ""}}</h3>

                                <p>{{isset($mot->contenu) ? (strlen(strip_tags($mot->contenu)) > 425) ? substr(strip_tags($mot->contenu, 0, 425)) . "..." : strip_tags($mot->contenu) : ""}}</p>

                                <a class="r-more " href="{{URL("—mot-du-recteur")}}">Lire la suite >></a>
                                <div class="clearfix">
                                    <div class="col-sm-6 ">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="clearfix"></div>
                <div class="">
                    <div class="headline">
                        <h3 class="light">A LA UNE</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 "> 
                        <?php
                        if (isset($actus)) {
                            ?>
                            <div class="owl-carousel2 row owl-loaded" >   
                                <?php
                                if (count($actus) > 0) {
                                    $in = -1;
                                    foreach ($actus as $cle => $actu) {
                                        $in++;
                                        if ($in == 5) {
                                            $in = 0;
                                        }
                                        $date1 = time();
                                        $diff_date = abs($date1 - strtotime($actu->created_at)) / (60 * 60 * 24);
                                        $diff_update = abs($date1 - strtotime($actu->updated_at)) / (60 * 60 * 24);
                                        $nc = "color" . $in;
                                        //$actu = $lactu;
                                        ?>
                                        <div class=" item  col-xs-1 col-md-4 ">
                                            <div class="news-v1-in borderd  {{$nc}}">
                                                <div class="cactu_image" >
                                                    <?php
                                                    if (isset($actu->photo) && !empty($actu->photo)) {
                                                        ?>
                                                        <a href="{{URL('activite/'.$actu->slug)}}">
                                                            <img class="img-responsive news-image " src="{{asset($actu->photo)}}" alt="{{isset($actu->titre) ? (strlen($actu->titre) > 50) ? substr($actu->titre, 0, 50) . "..." : $actu->titre : ""}}"></a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href="{{URL('activite/'.$actu->slug)}}">
                                                            <img class="img-responsive news-image " src="{{asset("assets/images/static/actu-newspaper.jpg")}}" alt="{{ isset($actu->titre) ? (strlen($actu->titre) > 50) ? substr($actu->titre, 0, 50) . "..." : $actu->titre : ""}}"></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                @if($diff_date<=3)
                                                <div class="actu_badge"><span class="label rond0 label-danger-2 fnormal"> Nouveau</span></div>
                                                @else
                                                @if($diff_update<=3)
                                                <div class="actu_badge"><span class="label rond0 label-success fnormal"> Mise à jour</span></div>
                                                @endif
                                                @endif
                                                <!--<div class="news-categorie bgcol1">Campus</div>-->
                                                <div class="pad5">
                                                    <div class="actu-titre">
                                                        <h4 class="light text-center">
                                                            <a  href="{{URL('activite/'.$actu->slug)}}">{{isset($actu->titre) ? substr($actu->titre,0, 42 ).'...' : ""}}</a></h4>
                                                    </div>
                                                    <p class="  actu-content  ">
                                                        <a href="{{URL('activite/'.$actu->slug)}}" class="text-muted">
                                                            {!! isset($actu->chapeau) ? substr(strip_tags($actu->chapeau),0, 110) : strip_tags(substr($actu->contenu, 100))!!}
                                                        </a>
                                                    </p>
                                                    <ul class="list-inline news-v1-info">

                                                        <li><span></span> <a href="#"><author>{{($actu->user())?$actu->user->nom.' '.$actu->user->prenom:''}}</author></a></li>
                                                        <!--<li>|</li>-->
                                                        <li><date>{{date('d/m/Y H:i',strtotime($actu->created_at))}}</date></li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="clearfix activite-link">
                            <div class="text-center ">
                                <?php
                                if (count($actus) > 1) {
                                    ?>
                                    <br><br>
                                    <a href="{{URL("activites")}}">
                                        <button class="btn btn-link xs-pull-left">
                                            <i class="glyphicon glyphicon-refresh "></i> Afficher +<span class="hidden-xs"> d'actualités</span>
                                            <!--<i class="glyphicon glyphicon-arrow"></i>-->
                                        </button>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4  ">               
                @include("includes.infosright") 
            </div>
        </div>
    </div>

</div>


@include('includes/home-3-step')

<div class="container bgwhite pad5 "> 

    @include('includes/bas-links')

</div>
@endsection