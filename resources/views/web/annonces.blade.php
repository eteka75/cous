@extends('layouts.web')
@section('title')
Toutes les annonces 
@endsection
@section('content')
@if(isset($annonces))
<div class="container bgwhite margin-top-5  ">
    <div class="row">
        <div class="col-lg-8 col-sm-8">
            <h3 class="light  page-header up-page-header">Annonces</h3>
             <form method="get" action="{{route('search')}}">
                <div class="input-group">
                    <input type="hidden" name="type" value="annonce">
                    <input type="text" name="q" class="form-control  search-control" placeholder="En quoi pouvons nous vous aider... ?">
                    <span class="input-group-btn">
                        <button class="btn btn-default search-btn"><i class="icon-magnifier"></i></button>
                    </span>
                </div>
            </form>
            <div class="pad10 bgwhite shadow1 main-content">
                <div class="menu-html-content mtop-10">
                            @if (isset($annonces) && $annonces->count()) 
                            <?php $pin = 1 ?>
                            <ul class='list-unstyled '>
                                @foreach ($annonces as $annonce) 
                                <?php
                                if ($pin == 6) {
                                    $in = 0;
                                }
                                $cln = "tcolor" . $pin++;
                                ?>
                                <li >
                                    <div class="row">
                                        <div class="col-sm-2 text-center f60">
                                            <i class="glyphicon glyphicon-bullhorn f60 {{$cln}}"></i>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class=" pad10">
                                                <h5><a href='{{url('annonce/'.$annonce->slug)}}'>{{$annonce->titre}} </a>
                                                    <small class="pull-right">
                                                        <date class="text-sm f11 text-muted">{{date('d/m/Y H:i',strtotime($annonce->created_at))}}</date>
                                                    </small></h5>
                                                <small class='text-xs text-muted pad0 text-right'>
                                                    <a href='{{url('annonce/'.$annonce->slug)}}'>{{substr($annonce->sous_titre,0,150)}}</a>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class='hr-xs'>
                                </li>
                                @endforeach
                            </ul>
                            <div class="text-right">
                               
                                {{$annonces->links()}}
                            </div>
                            @endif
                </div> 
            </div>
        </div>
        <div class="col-lg-4 col-sm-4   ">
            @include("includes/infosright");
        </div>
    </div>
</div>

@if($annonces->count()<=0)
<div class="clearfix text-center">
    <br><br><br>
    <h3 class="pad15 text-muted">Aucun évènement pour le moment</h3>
    <img src="{{asset('assets/images/resources/search.png')}}" alt="Aucun résultat">

</div>
<div class="text-center">
    <a class="btn btn-default  btn-sm" href="{{"login"}}">Suggérer un évènement</a>
</div>
@endif
@endif
@endsection
