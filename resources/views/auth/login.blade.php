@extends('layouts.web')

@section('content')
<style>
   
    .shadown1 {
        -moz-box-shadow: 0 0px 50px #aaa !important;
        -webkit-box-shadow: 0 0px 50px #aaa !important;
        box-shadow: 0 0px 50px #aaa !important;
    }
    span,label,.help-block{
        font-size:14px;
        text-align:left;
    }
    
</style>
<div class="mtop30"><br></div>
<div class="container bgwhite margin-top-50 mtop30 ">
    <div class="row">
        <div class="col-md-4  col-md-offset-4">

            <div class="">

                <div class="panel text-black text-center shadown_1 panel-default">
<!--                    <div class="text-center  pad15">
                        <div class="center"> <br><a href="{{url('/')}}">
                                <img alt="LERF-UP" src="{{asset('assets/images/logo.png')}}" class=" img-thumbnail"> </a></div>
                        <div class="lock-wrapper">
                            <h4 class="light"> COUS-PARAKOU  !</h4>
                        </div>
                        <p class="text-center text-muted">Créez votre compte aujourd'hui</p>
                    </div>-->
                    <h3 class="panel-headings text-black text-center light col-md-12 ">Connectez-vous !</h3>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <!--<label for="email" class="col-md-12 text-left">Votre e-mail</label>-->

                                <div class="col-md-12">
                                    <input id="email" type="email" placeholder="Courier électronique" class="input-lgs form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group borderTopc {{ $errors->has('password') ? ' has-error' : '' }}">
                                <!--<label for="password" class="col-md-12 text-left">Mot de passe</label>-->

                                <div class="col-md-12 ">
                                    <input id="password" type="password"  placeholder="Mot de passe" class="input-lgs form-control" name="password" required>

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-12 text-center pad-btm">
                                <label class="form-checkbox form-icon form-text">
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>  Rester connecté
                                </label>
                            </div>

                            <button type="submit" class="btn btn-block btn-primary">
                                Connexion
                            </button>
                    </div>
                    </form>
                </div>
            </div>
            <div>
                <div class="padd-all text-center">ou<br><br></div>
                <a class="btn btn-success btn-block btn-link" href="{{url('register')}}">Créer un compte</a>
                
            </div>
        </div>
    </div>
</div>
</div>


@endsection
