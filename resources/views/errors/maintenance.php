@extends('layouts.web')
@section('css')
<style>
    .nb-error {
        margin: 0 auto;
        text-align: center;
        max-width: 480px;
        padding: 0px 0px;
    }

    .nb-error .error-code {
        color: #2d353c;
        font-size: 96px;
        line-height: 100px;
    }

    .nb-error .error-desc {
        font-size: 12px;
        color: #647788;
    }
    .nb-error .input-group{
        margin: 30px 0;
    }
</style>
@endsection
@section('content')
<div class="nb-error">
    <div class="circle-user-login"><i class="fa text-white fa-eye-slash"></i></div>
    <div class="error-code">503</div>
    <h4 class="font-bold bold">Site en maintenance...</h4>

    <div class="error-desc">
        Désolé, la page que vous demandez n'existe pas ou a été déplacée. <br/>
       
        <form  action="{{route('search')}}" method="GET" role="search">
            <div class="input-group" >
                <input type="text" name="query" class="form-control" placeholder="Rechercher un sujet ..." name="q">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form>
        <ul class="list-inline text-center text-sm">
            <li class="list-inline-item"><a href="{{url('/')}}" class="text-muted">Retourner à l'accuiel</a>
            </li>
            <li class="list-inline-item"><a href="{{route('login')}}" class="text-muted">Se connecter</a>
            </li>
            <li class="list-inline-item"><a href="{{route('register')}}" class="text-muted">S'inscrire</a>
            </li>
        </ul>

    </div>
</div>
@endsection