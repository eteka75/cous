<?php
$mission = App\Page::where('slug', 'objectifs-et-missions')->first();
//$responsable = App\Page::where('slug', 'le-responsable')->first();
$mot = App\Page::where('slug', 'mot-de-bienvenue')->first();

$topActivites = App\Activite::latest()->take(5)->get();
$topDossiers = App\Dossier::latest()->take(5)->get();
$topComs = App\Communique::latest()->take(5)->get();
$topGals = App\Galerie::latest()->take(4)->get();
//dd($topActivites);
?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10&appId=1214967528615548";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div>
    @if(isset($mot) && $mot!=NULL)
    <div class="hentete">
        <h3 class="text-uppercase">Mot de bienvenue</h3>
    </div>
    <div class="lcard">
        {{substr(strip_tags($mot->contenu),0,200)}}<br>
        <a class="btn btn-defaultbtn-success text-danger btn-block" href="{{url('lerf/mot-de-bienvenue/')}}">Lire la suite >></a>
    </div>
    @endif
    <div class="mtop30 lcard">
        <div class="fb-page" data-href="https://www.facebook.com/COUS-Parakou-101765880579796/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/COUS-Parakou-101765880579796/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/COUS-Parakou-101765880579796/">COUS Parakou</a></blockquote></div>
    </div>
    <div class="lcard scard mtop30 rond3">
        <div class="widget-title"><div class="fa fa-folder icon"></div><h4><a class="rsswidget" href="#">Dernières activités</a></h4></div>
        @if(isset($topActivites) && $topActivites->count()>0)
        <div class="panel-body bg_geige mbottom20">
            <ul class="list-unstyleds m pad10 text-list light text-sm">
                @foreach($topActivites as $topActivite)
                <li>
                    <a class="text-muted " href="{{url('activite/'.$topActivite->slug)}}" title="{{$topActivite->titre}}"> {{str_limit($topActivite->titre,80)}}</a>
                   <br> <span class="text-sm"><a class="btn btn-xs btn-link" href="{{url('activite/'.$topActivite->slug)}}">lire la suite >></a></span>
                </li>
                @endforeach
        
        <a class="btn btn-sm btn-block btn-primary" href="{{URL('activites/')}}"> <i class="fa fa-plus-circle"></i> Plus d'activités</a>
            </div>
            
        @else
        <div class="pad15 text-muted text-center">
            <h1 class="huge-h-4"><i class="fa fa-tasks"></i></h1>
            <small class="text-muted bold">Aucun document pour le moment</small>
        </div>
        @endif
    </div>
    <div class="lcard scard rond3">
        <div class="widget-title  widget-warning"><div class="fa fa-folder-o icon"></div><h4><a class="rsswidget" href="#"> <a class="rsswidget" href="#">Derniers documents </a></h4></div>
        <div class="mbottom20 borderc ">
            @if(isset($topDossiers) && $topDossiers->count()>0)
            <div class="pad10">
            <ul class="list-unstyled text-sm text-muted">
                @foreach($topDossiers as $topDossier)
                <li class="pad5 borderBotc no-link">
                    <a href="{{url('dossiers/'.$topDossier->slug)}}"> <i class="fa fa-file-pdf-o"></i> {{$topDossier->titre}}</a>
                </li>
                @endforeach
            </ul>
            </div>
            @else
            <div class="pad15 text-muted text-center rond3">
                <h1 class="huge-h-4"><i class="fa fa-file-pdf-o"></i></h1>
                <small class="text-muted bold">Aucun document pour le moment</small>
            </div>
            @endif
        </div>
    </div>
    <div class="lcard scard rond3">
        <div class="widget-title  widget-danger"><div class="fa fa-info-circle icon"></div><h4><a class="rsswidget" href="#"> <a class="rsswidget" href="#">Communiqués</a></h4></div>
        <div class="panel-body bge mbottom20">
            @if(isset($topComs) && $topComs->count()>0)
            <ul class="list-unstyled text-list">
                @foreach($topComs as $topCom)

                <li>
                    <a href="{{url('communique/'.$topCom->slug)}}"> {{$topCom->titre}}</a>
                    <span class="text-sm"><a class="btn btn-xs btn-link" href="{{url('communique/'.$topCom->slug)}}">lire la suite >></a></span>
                </li>
                @endforeach
            </ul>
            <a class="btn btn-sm btn-block btn-default" href="{{URL('communiques/')}}"> <i class="fa fa-plus-circle"></i> Plus de communiqués</a>
            @else
            <div class="pad15 text-muted text-center">
                <h1 class="huge-h-4"><i class="fa fa-info-circle"></i></h1>
                <small class="text-muted bold">Aucun communiqué</small>
            </div>
           @endif

        </div>
    </div>
    <div class="lcard scard hidden ">
        <div class="widget-title  widget-default"><div class="fa fa-picture-o icon"></div><h4><a class="rsswidget" href="#"> <a class="rsswidget" href="#">Galerie images</a></h4></div>
    </div>
    <div class="borderc hidden text-muted">
        <div class="pad15 text-muted text-center">
            <h1 class="huge-h-4"><i class="fa fa-image"></i></h1>
            <small class="text-muted bold">Aucune image </small>
        </div>
    </div>

   
    @if(isset($mission) && $mission!=NULL)
    <div class="hentete">
        <h3>Notre Mission</h3>
    </div>

    <div class="lcard">
        {{substr(strip_tags($mission->contenu),0,300)}}
        <br>
        <a class="text-danger" href="{{url('/objectifs-et-missions.html')}}">Plus de détails >></a>
    </div>
    @endif
    <div class="lcard">

    </div>
    
</div>
<br>