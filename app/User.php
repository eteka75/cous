<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Berkayoztunc\LaravelProfile;
use Berkayoztunc\LaravelProfile\Model\RecordCreated;
use Berkayoztunc\LaravelProfile\Model\RecordDeleted;
use Berkayoztunc\LaravelProfile\Model\RecordUpdated;

class User extends Authenticatable
{
    use Notifiable, HasRoles,LaravelProfile\Trackable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom','sexe','telephone', 'email', 'password','photo','username','about','is_admin','is_editor'
    ];
     
    /**
    * exp : User will "My awesome user model"
    **/
    public $tracingName = 'Utlisateur';
   
    protected $events = [
        'Inscription' => RecordCreated::class,
        'Déconnexion' => RecordDeleted::class,
        'Mise à jour du compte' => RecordUpdated::class,
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
