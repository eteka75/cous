<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Etude;
use Illuminate\Http\Request;
use Session;

class EtudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $etudes = Etude::where('titre', 'LIKE', "%$keyword%")
				->orWhere('photo', 'LIKE', "%$keyword%")
				->orWhere('legende', 'LIKE', "%$keyword%")
				->orWhere('detail', 'LIKE', "%$keyword%")
				->orWhere('user_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $etudes = Etude::paginate($perPage);
        }

        return view('admin.etudes.index', compact('etudes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.etudes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'photo' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('photo')) {
            $url = '/uploads/activites/';
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        Etude::create($requestData);

        Session::flash('flash_message', 'Etude added!');

        return redirect('admin/etudes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $etude = Etude::findOrFail($id);

        return view('admin.etudes.show', compact('etude'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $etude = Etude::findOrFail($id);

        return view('admin.etudes.edit', compact('etude'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
//			'photo' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('photo')) {
            $url = '/uploads/activites/';
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        $etude = Etude::findOrFail($id);
        $etude->update($requestData);

        Session::flash('flash_message', 'Etude updated!');

        return redirect('admin/etudes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Etude::destroy($id);

        Session::flash('flash_message', 'Etude deleted!');

        return redirect('admin/etudes');
    }
}
