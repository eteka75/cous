<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\TitrePage;
use DB;

class PageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 12;

        if (!empty($keyword)) {
            $page = Page::where('titre', 'LIKE', "%$keyword%")
                    ->orWhere('slug', 'LIKE', "%$keyword%")
                    ->orWhere('contenu', 'LIKE', "%$keyword%")
                    ->paginate($perPage);
        } else {
            $page = Page::paginate($perPage);
        }

        return view('admin.page.index', compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $allslug = Page::pluck('slug')->all();
        $pages = TitrePage::orderBy('titre')->whereNotIn('slug', $allslug)->pluck('titre', 'slug');

        return view('admin.page.create', compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'titre' => 'required',
            'slug' => 'required',
            'contenu' => 'required'
        ]);
        $requestData = $request->all();
        if ($request->hasFile('photo') &&  intval($request->input('has_photo'))>0){
            $url = '/uploads/' . config('app.dossiers.page');
            $uploadPath = public_path($url);

            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }
            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        if($request->hasFile('has_photo')=='0'){
             $requestData['photo']='';
        }
        
        if (Auth::check()) {
            $requestData['user_id'] = Auth::user()->id;
        }
        Page::create($requestData);

        Session::flash('flash_message', 'Page ajoutée avec succès!');

        return redirect('admin/page');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $page = Page::findOrFail($id);

        return view('admin.page.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $page = Page::findOrFail($id);
//        dd($page);
         $allslug = Page::pluck('slug')->all();
        $pages = TitrePage::orderBy('titre')->whereIn('slug', $allslug)->pluck('titre', 'slug');

        return view('admin.page.edit', compact('page','pages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'titre' => 'required',
            'slug' => 'required',
            'contenu' => 'required'
        ]);
        $requestData = $request->all();

        $requestData['user_id'] = 0;
        if (Auth::check()) {
            $requestData['user_id'] = Auth::user()->id;
        }

        $page = Page::findOrFail($id);

        if ($request->hasFile('photo') &&  intval($request->input('has_photo'))>0){
            $url = '/uploads/' . config('app.dossiers.page');
            $uploadPath = public_path($url);

            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }
            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        if($request->input('has_photo')=="0"){
             $requestData['photo']='';
        }
       
        $page->update($requestData);

        Session::flash('flash_message', 'Page mise à jour avec succès!');

        return redirect('admin/page');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Page::destroy($id);

        Session::flash('flash_message', 'Page supprimée avec succès!');

        return redirect('admin/page');
    }

}
