<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Slide;
use Illuminate\Http\Request;
use Session;
use Auth;

class SlidesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $slides = Slide::where('photo', 'LIKE', "%$keyword%")
                    ->orWhere('titre', 'LIKE', "%$keyword%")
                    ->orWhere('sous_titre', 'LIKE', "%$keyword%")
                    ->orWhere('detail', 'LIKE', "%$keyword%")
                    ->paginate($perPage);
        } else {
            $slides = Slide::paginate($perPage);
        }

        return view('admin.slides.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'photo' => 'required',
            'titre' => 'required'
        ]);
        $requestData = $request->all();
        if ($request->hasFile('photo')) {
            $url = '/uploads/slides/' ;
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }
            $uploadPath = public_path($url);

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        if (Auth::check()) {
            $requestData['user_id'] = Auth::user()->id;
        }
        Slide::create($requestData);

        Session::flash('flash_message', 'Slide added!');

        return redirect('admin/slides');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $slide = Slide::findOrFail($id);

        return view('admin.slides.show', compact('slide'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $slide = Slide::findOrFail($id);

        return view('admin.slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {
        $this->validate($request, [
//            'photo' => 'required',
            'titre' => 'required'
        ]);
        $requestData = $request->all();
        if ($request->hasFile('photo')) {
            $url = '/uploads/' . config('app.dossiers.slide');
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }
            $uploadPath = public_path($url);

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        if(Auth::check()){$requestData['user_id']=Auth::user()->id;}
        $slide = Slide::findOrFail($id);
        $slide->update($requestData);

        Session::flash('flash_message', 'Slide updated!');

        return redirect('admin/slides');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Slide::destroy($id);

        Session::flash('flash_message', 'Slide deleted!');

        return redirect('admin/slides');
    }

}
