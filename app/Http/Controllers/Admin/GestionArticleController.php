<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use Illuminate\Http\Request;
use Session;
use App\CategorieArticle;
use Auth;

class GestionArticleController extends Controller {

    public function getValidate($id) {
        if (Auth::user()->hasRole('admin') || Auth::user()->can('validate_article')) {

            $article = Article::findOrFail($id);
            $article->etat = '1';
            $article->save();
            Session::flash('flash_message', 'Validation réussie!');
        }

        return redirect()->back();
    }

    public function getUnvalidate($id) {
       if (Auth::user()->hasRole('admin') || Auth::user()->can('validate_article')) {
            $article = Article::findOrFail($id);
            $article->etat = '0';
            $article->save();
            Session::flash('flash_message', 'Validation annulée!');
        }
        return redirect()->back();
    }

}
