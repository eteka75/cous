<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Galerie;
use Illuminate\Http\Request;
use Session;
use Auth;
use Image;

class GalerieController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $galerie = Galerie::where('image', 'LIKE', "%$keyword%")
                    ->orWhere('titre', 'LIKE', "%$keyword%")
                    ->orWhere('detail', 'LIKE', "%$keyword%")
                    ->orWhere('active', 'LIKE', "%$keyword%")
                    ->orWhere('url', 'LIKE', "%$keyword%")
                    ->paginate($perPage);
        } else {
            $galerie = Galerie::paginate($perPage);
        }

        return view('admin.galerie.index', compact('galerie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.galerie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'image' => 'required', //mimes:jpeg,bmp,png,jpg,gif
            'titre' => 'required|min:5|max:200'
        ]);
        $requestData = $request->all();
        $requestData['images'] = '';
        if ($request->hasFile('image')) {
//           dd($request['images']);
            $url = 'uploads/galerie/images/';
            $uploadPath = public_path($url);
            if (!is_dir($uploadPath)) {
                @mkdir($uploadPath);
            }
            foreach ($request->image as $file) {

                $extension = $file->getClientOriginalExtension();
                $fileName = "cous_parakou_galerie_" . rand(11111, 999999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
//                 dd($uploadPath);
                $requestData['images'] .= $url . $fileName . ';';
            }
        }
        if (Auth::check()) {
            $requestData['user_id'] = Auth::user()->id;
        } else {
            abort(404);
        }
        Galerie::create($requestData);

        Session::flash('flash_message', 'Ajout effectué à la galerie!');

        return redirect('admin/galerie');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $galerie = Galerie::findOrFail($id);

        return view('admin.galerie.show', compact('galerie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $galerie = Galerie::findOrFail($id);

        return view('admin.galerie.edit', compact('galerie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'image' => 'required',
            'titre' => 'required|min:5'
        ]);
        $requestData = $request->all();

        $galerie = Galerie::findOrFail($id);
        $requestData['images'] = '';
        if ($request->hasFile('image')) {
            foreach ($request['image'] as $file) {
                $url = 'uploads/galerie/images/';
                $uploadPath = public_path($url);
                if (!is_dir($uploadPath)) {
                    @mkdir($uploadPath);
                }
                $extension = $file->getClientOriginalExtension();
                $fileName = "cous_parakou_galerie_" . rand(11111, 999999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['images'] .= $url . $fileName . ';';
            }
        }
        $galerie->update($requestData);

        Session::flash('flash_message', 'Galerie mis à jour !');

        return redirect('admin/galerie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Galerie::destroy($id);

        Session::flash('flash_message', 'Suppression effectuée !');

        return redirect('admin/galerie');
    }

}
