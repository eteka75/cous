<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'personnels';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom_prenom', 'fonction','categorie','specialite', 'grade', 'biographie', 'adresse', 'photo'];

    
}
